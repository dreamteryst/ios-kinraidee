//
//  User.swift
//  Kinraidee
//
//  Created by admin on 11/6/2561 BE.
//  Copyright © 2561 dreamerdev. All rights reserved.
//

import Foundation

class User: NSObject {
    var id: Int
    var username: String
    var chats: [Chat]
    
    init(id: Int, username: String, chats: [Chat]) {
        self.id = id
        self.username = username
        self.chats = chats
    }
}
