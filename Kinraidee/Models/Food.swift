//
//  Food.swift
//  Kinraidee
//
//  Created by admin on 5/6/2561 BE.
//  Copyright © 2561 dreamerdev. All rights reserved.
//

import Foundation
class Food: NSObject {
    var id: String
    var name: String
    var price: Int
    var imageUrl: String
    
    init(id: String, name: String, price: Int, imageUrl: String) {
        self.id = id
        self.name = name
        self.price = price
        self.imageUrl = imageUrl
        
        super.init()
    }
}
