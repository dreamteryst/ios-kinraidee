//
//  Chat.swift
//  Kinraidee
//
//  Created by admin on 11/6/2561 BE.
//  Copyright © 2561 dreamerdev. All rights reserved.
//

import Foundation

class Chat: NSObject {
    var username: String
    var message: String
    
    init(username: String, message: String) {
        self.username = username
        self.message = message
    }
}
