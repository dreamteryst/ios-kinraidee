//
//  MapViewController.swift
//  Kinraidee
//
//  Created by admin on 11/6/2561 BE.
//  Copyright © 2561 dreamerdev. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON

class MapViewController: UIViewController, CLLocationManagerDelegate {

    var markerDict: [String: GMSMarker] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func loadView() {
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        let camera = GMSCameraPosition.camera(withLatitude: 13.6793712, longitude: 100.627404, zoom: 17.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
//        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 50)
        
        
        
        view = mapView
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: 13.6793712, longitude: 100.627404)
        marker.title = "Start"
        marker.snippet = "BKK"
        marker.map = mapView
        markerDict["start"] = marker
        let endMarker = GMSMarker()
        endMarker.position = CLLocationCoordinate2D(latitude: 13.6952004, longitude: 100.6491033)
        endMarker.title = "End"
        endMarker.snippet = "BKK"
        endMarker.map = mapView
        markerDict["end"] = endMarker
        
        let origin = "\(13.6793712),\(100.627404)"
        let destination = "\(13.6952004),\(100.6491033)"
        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=AIzaSyASfk08XXlNAsxgzopXm_neHUjivk7zeX0"
        
        Alamofire.request(url).responseJSON { response in
            let json = try? JSON(data: response.data!)
            let routes = json!["routes"].arrayValue
            
            for route in routes
            {
                let routeOverviewPolyline = route["overview_polyline"].dictionary
                let points = routeOverviewPolyline?["points"]?.stringValue
                let path = GMSPath.init(fromEncodedPath: points!)
                
                let polyline = GMSPolyline(path: path)
                polyline.strokeColor = UIColor(rgb: 0x0066FF)
                polyline.strokeWidth = 5.0
                polyline.map = mapView
                
            }
        }
        
    }

}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
