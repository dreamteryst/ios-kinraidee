//
//  ChatViewController.swift
//  Kinraidee
//
//  Created by admin on 11/6/2561 BE.
//  Copyright © 2561 dreamerdev. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelMessage: UILabel!
    
}

class ChatViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    let chatViewModel: ChatViewModel = ChatViewModel()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textMessage: UITextField!
    let cellReuseIdentifier = "cell"
    
    
    @IBAction func sendMessage(_ sender: Any) {
        if(textMessage.text == "") {
            return
        }
        self.chatViewModel.sendMessage(message: self.textMessage.text!) {
            self.textMessage.text = ""
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.chatViewModel.handleSocket {
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatViewModel.chats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: ChatTableViewCell!
        if(self.chatViewModel.chats[indexPath.row].username == self.chatViewModel.username) {
            // create a new cell if needed or reuse an old one
            cell = (self.tableView.dequeueReusableCell(withIdentifier: "cellRight") as! ChatTableViewCell?)!
            
            // set the text from the data model
            cell.imageAvatar!.image = UIImage(named: "logo")
            cell.labelUsername!.text = self.chatViewModel.chats[indexPath.row].username
            cell.labelMessage!.text = self.chatViewModel.chats[indexPath.row].message
        } else {
            // create a new cell if needed or reuse an old one
            cell = (self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as! ChatTableViewCell?)!
            
            // set the text from the data model
            cell.imageAvatar!.image = UIImage(named: "logo")
            cell.labelUsername!.text = self.chatViewModel.chats[indexPath.row].username
            cell.labelMessage!.text = self.chatViewModel.chats[indexPath.row].message
        }
        
//        cell.labelMessage!.layer.backgroundColor = UIColor(red:0.00, green:0.60, blue:1.00, alpha:1.0).cgColor
//        cell.labelMessage!.textColor = UIColor(red: 1.00, green: 1.00, blue: 1.00, alpha: 1.00)
//        cell.labelMessage!.layer.cornerRadius = 5
        
        
        return cell!
    }
    
}

