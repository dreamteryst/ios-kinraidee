//
//  ChatViewModel.swift
//  Kinraidee
//
//  Created by admin on 11/6/2561 BE.
//  Copyright © 2561 dreamerdev. All rights reserved.
//

import Foundation
import SocketIO
import SwiftyJSON

class ChatViewModel {
    var chats: [Chat] = [Chat]()
    var username: String = ""
    var numUsers: Int = 0
    let manager = SocketManager(socketURL: URL(string: "http://192.168.1.114:3000")!, config: [.log(true), .compress])
}

extension ChatViewModel {
    func handleSocket(completion: (() -> Void)? = nil) {
        let socket = manager.defaultSocket
        
        socket.on(clientEvent: .connect) {data, ack in
            print("socket connected")
            self.newUser(username: "dreamteryst")
        }
        
        socket.on("new message") { data, ack in
            if let arr = data as? [[String: Any]] {
                let chat = Chat(username: arr[0]["username"] as! String,
                                message: arr[0]["message"] as! String)
                self.chats.append(chat)
            }
            // callback
            completion?()
        }
        
        socket.on("user joined") { data, ack in
            if let arr = data as? [[String: Any]] {
                self.numUsers = arr[0]["numUsers"] as! Int
            }
            // callback
            completion?()
        }
        
        socket.on("user left") { data, ack in
            completion?()
        }
        
        socket.on("login") { data, ack in
            if let arr = data as? [[String: Any]] {
                if let numUsers = arr[0]["numUsers"] as? Int {
                    self.numUsers = numUsers
                }
            }
            // callback
            completion?()
        }
        
        socket.connect()
    }
    
    func newUser(username: String) {
        let socket = manager.defaultSocket
        self.username = "\(username)\(self.numUsers)"
        socket.emit("add user", self.username)
    }
    
    func sendMessage(message: String, completion: (() -> Void)? = nil) {
        let socket = manager.defaultSocket
        socket.emit("new message", message)
        let chat = Chat(username: self.username, message: message)
        self.chats.append(chat)
        completion?()
    }
}
