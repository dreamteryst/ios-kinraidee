//
//  AppModelView.swift
//  Kinraidee
//
//  Created by admin on 5/6/2561 BE.
//  Copyright © 2561 dreamerdev. All rights reserved.
//
import Foundation
import Firebase

class FoodListViewModel {
    
    var title :String? = "Foods"
    var name: Box<String?> = Box(nil)
    var price: Box<String> = Box("")
    var imageUrl: Box<URL?> = Box(nil)
    var foods :[Food] = [Food]()
    var timeDelay = 0.1
    var count = 1
    var loading: Box<Bool> = Box(false)
}

extension FoodListViewModel {
    
    func loadData(completion: (() -> Void)? = nil) {
        self.loading.value = true
        let ref: DatabaseReference! = Database.database().reference()
        ref.child("foods").observe(DataEventType.value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                self.foods = [Food]()
                for food in snapshot.children.allObjects as! [DataSnapshot] {
                    let foodObject = food.value as? [String: AnyObject]
                    let foodModel = Food(id: food.key, name: foodObject!["name"] as! String, price: foodObject!["price"] as! Int, imageUrl: foodObject!["imageUrl"] as! String)
                    self.foods.append(foodModel)
                }
                self.loading.value = false
                completion?()
            }
        })
        
    }
    
    func randomize() {
        self.loading.value = true
        if self.count == 20 {
            self.count = 1
            self.timeDelay = 0.1
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + self.timeDelay) {
            let randomIndex = Int(arc4random_uniform(UInt32(self.foods.count)))
            let url = URL(string: self.foods[randomIndex].imageUrl)!
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = NumberFormatter.Style.decimal
            self.name.value = self.foods[randomIndex].name
            self.price.value = numberFormatter.string(from: NSNumber(value:self.foods[randomIndex].price))!
            self.imageUrl.value = url
            
            if self.count != 20 {
                self.randomize()
                self.count += 1
                self.timeDelay += 0.01
            } else {
                self.loading.value = false
            }
        }
    }
    
}
