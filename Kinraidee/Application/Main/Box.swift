//
//  Box.swift
//  Kinraidee
//
//  Created by admin on 7/6/2561 BE.
//  Copyright © 2561 dreamerdev. All rights reserved.
//

import Foundation

class Box<T> {
    typealias Listener = (T) -> Void
    var listener: Listener?
    
    var value: T {
        didSet{
            listener? (value)
        }
    }
    
    init(_ value: T) {
        self.value = value
    }
    
    func bind(listener: Listener?) {
        self.listener = listener
        listener?(value)
    }
}
