//
//  ViewController.swift
//  Kinraidee
//
//  Created by admin on 5/6/2561 BE.
//  Copyright © 2561 dreamerdev. All rights reserved.
//

import UIKit
import Firebase
import Kingfisher

class ViewController: UIViewController {
    
    @IBOutlet weak var btnRadom: UIButton!
    @IBOutlet weak var btnAddFood: UIButton!
    @IBOutlet weak var imgFood: UIImageView!
    @IBOutlet weak var labelFoodName: UILabel!
    @IBOutlet weak var labelFoodPrice: UILabel!
    
    var food: [Food] = []
    var foodViewModel: FoodListViewModel = FoodListViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        btnRadom.layer.cornerRadius = 20
        
        self.becomeFirstResponder() // To get shake gesture
        foodViewModel.loadData()
        foodViewModel.name.bind{ [unowned self] in
            self.labelFoodName.text = $0
        }
        foodViewModel.price.bind{ [unowned self] in
            if $0 == "" {
                self.labelFoodPrice.text = "คลิก Random เพื่อทำการสุ่ม"
            } else {
                self.labelFoodPrice.text = "ราคา \($0)  บาท"
            }
        }
        foodViewModel.imageUrl.bind{ [unowned self] in
            if $0 != nil {
                self.imgFood.kf.setImage(with: $0)
            }
        }
        foodViewModel.loading.bind{ [unowned self] in
            if $0 {
                self.btnRadom.alpha = 0.5
            } else {
                self.btnRadom.alpha = 1
            }
            self.btnRadom.isEnabled = !$0
        }
        
    }
    
    
    @IBAction func clickRandom(_ sender: Any) {
        self.foodViewModel.randomize()
    }
    
    @IBAction func goAddFood(_ sender: Any) {
        
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "addFoodPage") as? AddFoodViewController {
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // We are willing to become first responder to get shake motion
    override var canBecomeFirstResponder: Bool {
        get {
            return true
        }
    }
    
    // Enable detection of shake motion
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            self.foodViewModel.randomize()
        }
    }
    
    
    @IBAction func goChat(_ sender: Any) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "chatPage") as? ChatViewController {
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }


}

