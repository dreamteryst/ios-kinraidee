//
//  ShowFoodViewController.swift
//  Kinraidee
//
//  Created by admin on 7/6/2561 BE.
//  Copyright © 2561 dreamerdev. All rights reserved.
//

import UIKit
import Kingfisher

class ShowFoodViewController: UIViewController {
    @IBOutlet weak var imageFood: UIImageView!
    @IBOutlet weak var labelFoodName: UILabel!
    @IBOutlet weak var labelFoodPrice: UILabel!
    
    var selectedRow: Int! = -1
    var foodViewModel: FoodListViewModel = FoodListViewModel()
    
    @IBAction func goMap(_ sender: Any) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mapPage") as? MapViewController {
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        foodViewModel.loadData {
            self.title = self.foodViewModel.foods[self.selectedRow].name
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = NumberFormatter.Style.decimal
            let url = URL(string: self.foodViewModel.foods[self.selectedRow].imageUrl)
            self.imageFood.kf.setImage(with: url)
            self.labelFoodName.text = self.foodViewModel.foods[self.selectedRow].name
            self.labelFoodPrice.text = "ราคา \(String(describing: numberFormatter.string(from: NSNumber(value:self.foodViewModel.foods[self.selectedRow].price))!)) บาท"
        }
        
        

        // Do any additional setup after loading the view.
    }

}
