//
//  AddFoodViewController.swift
//  Kinraidee
//
//  Created by admin on 5/6/2561 BE.
//  Copyright © 2561 dreamerdev. All rights reserved.
//

import UIKit
import Firebase
import ImagePicker

class AddFoodViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ImagePickerDelegate  {
    
    @IBOutlet weak var textName: UITextField!
    @IBOutlet weak var textPrice: UITextField!
    @IBOutlet weak var textImage: UITextField!
    @IBOutlet weak var btnSave: UIBarButtonItem!
    @IBOutlet weak var tableFoods: UITableView!
    
    let cellReuseIdentifier = "cell"
    
    var ref: DatabaseReference!
    var foodViewModel: FoodListViewModel = FoodListViewModel()
    
    var activityIndicator = UIActivityIndicatorView()
    var strLabel = UILabel()
    
    let effectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "เพิ่มเมนูอาหาร"
        self.foodViewModel.loadData {
             self.tableFoods.reloadData()
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saving(_ sender: Any) {
        
        if self.textName.text == "" || self.textPrice.text == "" || self.textImage.text == "" {
            let alert = UIAlertController(title: "Alert", message: "Please fill required field", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return;
        }
        
        self.ref = Database.database().reference()
        self.ref.child("foods").childByAutoId().setValue([
            "name": self.textName.text ?? "",
            "price": Int(self.textPrice.text!) ?? 0,
            "imageUrl": self.textImage.text ?? ""
            ], withCompletionBlock: {
            (error, reference) in
            if error != nil {
                let alert = UIAlertController(title: "Error", message: error as? String, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                self.textName.text = ""
                self.textPrice.text = ""
                self.textImage.text = ""
                self.textImage.isUserInteractionEnabled = true
                let alert = UIAlertController(title: "Added", message: "Add food successful", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.foodViewModel.foods.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell:UITableViewCell = (self.tableFoods.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell?)!
        
        // set the text from the data model
        cell.textLabel?.text = self.foodViewModel.foods[indexPath.row].name
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "showFoodPage") as? ShowFoodViewController {
            viewController.selectedRow = indexPath.row
            if let navigator = navigationController {
                navigator.pushViewController(viewController, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let ref = Database.database().reference()
        let item = ref.child("foods").child(self.foodViewModel.foods[indexPath.row].id)
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            item.removeValue()
            print("Deleting \(self.foodViewModel.foods[indexPath.row].id)")
            // handle delete (by removing the data from your array and updating the tableview)
        }
    }
    
    @IBAction func upload(_ sender: Any) {
//        let imagePicker = UIImagePickerController()
//        imagePicker.delegate = self
//        present(imagePicker, animated: true, completion: nil)
        let imagePickerController = ImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.imageLimit = 1
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        imagePicker.dismiss(animated: true, completion: nil)
        activityIndicator("Uploading")
        let uploadMetaData = StorageMetadata()
        let storage = Storage.storage()
        let storageRef = storage.reference()
        let data = UIImageJPEGRepresentation(images[0], 1)
        let timeInterval = NSDate().timeIntervalSince1970
        
        uploadMetaData.contentType = "image/jpeg"
        storageRef.child("images").child("\(timeInterval).jpg").putData(data!, metadata: uploadMetaData) {
            metadata, error in
            // You can also access to download URL after upload.
            storageRef.child("images").child("\(timeInterval).jpg").downloadURL { (url, error) in
                guard let downloadURL = url else {
                    // Uh-oh, an error occurred!
                    return
                }
                self.textImage.text = downloadURL.absoluteString
                self.textImage.isUserInteractionEnabled = false
                self.effectView.removeFromSuperview()
            }
        }
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    private func activityIndicator(_ title: String) {
        
        strLabel.removeFromSuperview()
        activityIndicator.removeFromSuperview()
        effectView.removeFromSuperview()
        
        strLabel = UILabel(frame: CGRect(x: 50, y: 0, width: 160, height: 46))
        strLabel.text = title
        strLabel.font = .systemFont(ofSize: 14, weight: .medium)
        strLabel.textColor = UIColor(white: 0.9, alpha: 0.7)
        
        effectView.frame = CGRect(x: view.frame.midX - strLabel.frame.width/2, y: view.frame.midY - strLabel.frame.height/2 , width: 160, height: 46)
        effectView.layer.cornerRadius = 15
        effectView.layer.masksToBounds = true
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
        activityIndicator.startAnimating()
        
        effectView.contentView.addSubview(activityIndicator)
        effectView.contentView.addSubview(strLabel)
        view.addSubview(effectView)
    }
    

}
